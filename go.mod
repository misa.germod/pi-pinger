module pi-pinger

go 1.16

require (
	github.com/go-ping/ping v1.1.0
	github.com/stianeikeland/go-rpio v4.2.0+incompatible // indirect
)
