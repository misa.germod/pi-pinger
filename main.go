package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-ping/ping"
	rpio "github.com/stianeikeland/go-rpio"
)

func main() {
	var (
		pins          ArrayFlags
		timeoutSecond int
	)
	// вичитка конфігурації
	flag.Var(&pins, "pins", "example 4:100:1.1.1.1,8.8.8.8")
	flag.IntVar(&timeoutSecond, "timeoutSecond", 3, "by default 3s")
	flag.Parse()

	// ініціалізація GPIO
	if err := rpio.Open(); err != nil {
		log.Fatalln(err)
	}

	// запуск моніторингу
	for _, pin := range pins {
		cfg := parseIndicatorConfiguration(pin)
		if cfg != nil {
			go runIndicator(cfg, time.Duration(timeoutSecond)*time.Second)
		}
	}

	// очікування команди закінчення роботи
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill, syscall.SIGTERM)
	go func() {
		<-c
		os.Exit(0)
	}()

	select {}
}

func runIndicator(configuration *IndicatorConfiguration, timeout time.Duration) {
	var (
		redPin = rpio.Pin(configuration.Pin)
	)

	// змінюємо режим на output
	redPin.Output()

	// вимикаємо
	redPin.Low()

	for {
		status := pingAddresses(configuration.Hosts, configuration.MaxLossPercent, timeout)
		log.Println(configuration.Hosts, status)

		if status { // все гуд, подаємо живлення
			redPin.High()
			continue
		}

		// тест зафейлився, вимикаємо
		redPin.Low()
	}
}

// addrs - масив ip адрес або доменів(можна змішувати)
// lossPercent - % втрачених пакетів, 100 == всі пакети втрачено
// timeout - таймаут тесту
func pingAddresses(addrs []string, lossPercent int, timeout time.Duration) (status bool) {
	var (
		// канал по якому буде надходити статистика виконань
		statisticChan = make(chan *ping.Statistics)
		// каунтер що б виявити момент коли 100% інформації надійшло
		counter = 0
	)

	// закриття каналу після виконання функції
	defer close(statisticChan)

	// запуск перебору в паралельному режимі
	for _, addr := range addrs {
		go func(ch chan *ping.Statistics, addr string, t time.Duration) {
			ch <- pingAddr(addr, timeout)
		}(statisticChan, addr, timeout)
	}

	// отримання статусів
	for statistic := range statisticChan {
		// все ок, нетворк доступний
		if statistic != nil && statistic.PacketLoss <= float64(lossPercent) {
			status = true
		}
		counter++

		// всі статуси надійшли
		if counter == len(addrs) {
			break
		}
	}

	return status
}

func pingAddr(addr string, timeout time.Duration) *ping.Statistics {
	pinger, err := ping.NewPinger(addr)
	if err != nil {
		log.Println("fail to init ping client, err:", err)
		time.Sleep(timeout)
		return nil
	}
	pinger.SetPrivileged(true)
	pinger.Timeout = timeout
	err = pinger.Run()
	if err != nil {
		log.Println("fail to run ping test, err:", err)
		time.Sleep(timeout)
		return nil
	}
	return pinger.Statistics()
}
