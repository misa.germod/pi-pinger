package main

import (
	"regexp"
	"strconv"
	"strings"
)

type ArrayFlags []string

func (a *ArrayFlags) String() string {
	return strings.Join(*a, ",")
}

func (a *ArrayFlags) Set(value string) error {
	*a = append(*a, value)
	return nil
}

type IndicatorConfiguration struct {
	Pin            int
	MaxLossPercent int
	Hosts          []string
}

func parseIndicatorConfiguration(val string) *IndicatorConfiguration {
	var (
		matches = regexp.MustCompile(`(\d+):(\d+):(.*)`).FindStringSubmatch(val)
	)

	if len(matches) == 0 {
		return nil
	}

	pinNumber, _ := strconv.Atoi(matches[1])
	maxLossPercent, _ := strconv.Atoi(matches[2])

	return &IndicatorConfiguration{
		Pin:            pinNumber,
		MaxLossPercent: maxLossPercent,
		Hosts:          strings.Split(matches[3], ","),
	}
}
